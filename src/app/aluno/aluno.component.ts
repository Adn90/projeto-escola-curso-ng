import { Component, OnInit } from "@angular/core";
import { Aluno } from "./Aluno";
import { Router } from "@angular/router";
import { AlunoService } from "./servico/aluno.service";

@Component({
  selector: "app-aluno",
  templateUrl: "./aluno.component.html",
  styleUrls: ["./aluno.component.scss"]
})
export class AlunoComponent implements OnInit {
  aluno = new Aluno();
  alunos: Aluno[] = [];
  alunoSelecionado:Aluno;

  constructor(private _router: Router, private _alunoservico: AlunoService) {}

  ngOnInit(): void {
    this.pesquisar();
  }

  pesquisar() {
    this._alunoservico.pesquisar(this.aluno.nome).subscribe((alunos: Aluno[]) => {
      this.alunos = alunos;
    })
  }

  incluir() {
    this._router.navigate(['/aluno/incluir'])
  }

  selecionar(aluno: Aluno) {
    this.alunoSelecionado = aluno;
  }

  remover() {
    this._alunoservico.excluir(this.alunoSelecionado).subscribe(aluno => {
      alert(aluno['mensagem']);
    })
  }

  alterar() {
    if (this.alunoSelecionado) {
      this._router.navigate(['/aluno/alterar/'+this.alunoSelecionado.nome]);
    }else {
      alert("Selecione um aluno!");
    }
  }
}
