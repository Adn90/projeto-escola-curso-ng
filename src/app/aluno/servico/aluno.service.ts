import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Aluno } from "../Aluno";

@Injectable({
  providedIn: "root"
})
export class AlunoService {
  constructor(private _http: HttpClient) {}

  pesquisar(nome: string) {
    return this._http.get(
      "https://cors-anywhere.herokuapp.com/https://stormy-badlands-29216.herokuapp.com/api/aluno/consultar/" +
        nome
    );
  }

  incluir(aluno: Aluno) {
    return this._http.post(
      "https://cors-anywhere.herokuapp.com/https://stormy-badlands-29216.herokuapp.com/api/aluno/incluir",
      aluno
    );
  }

  excluir(aluno: Aluno) {
    return this._http.post(
      "https://cors-anywhere.herokuapp.com/https://stormy-badlands-29216.herokuapp.com/api/aluno/remover",
      aluno
    );
  }

  alterar(aluno: Aluno) {
    return this._http.patch(
      "https://cors-anywhere.herokuapp.com/https://stormy-badlands-29216.herokuapp.com/api/aluno/alterarparcial",
      aluno
    );
  }
}
