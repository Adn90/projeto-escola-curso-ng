import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { Aluno } from "../Aluno";
import { AlunoService } from "../servico/aluno.service";

@Component({
  selector: "app-aluno-manter",
  templateUrl: "./aluno-manter.component.html",
  styleUrls: ["./aluno-manter.component.scss"]
})
export class AlunoManterComponent implements OnInit {
  aluno = new Aluno();
  operacao: string = "incluir";
  alunoEditar: string = '';
  constructor(
    private _alunoServico: AlunoService,
    private _router: Router,
    private _activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.alunoEditar = this._activatedRoute.snapshot.params.id;
    if (this.alunoEditar != null) {
      this.operacao = "Alterar";
      this._alunoServico.pesquisar(this.alunoEditar).subscribe(aluno => {
        this.aluno = (<Aluno[]>aluno)[0];
      });
    }
  }

  salvar() {
    this._alunoServico.incluir(this.aluno).subscribe(aluno => {
      alert(aluno["mensagem"]);
      this.voltar();
    });
  }

  voltar() {
    this._router.navigate(["/aluno"]);
  }

  deletar() {
    this._alunoServico.excluir(this.aluno).subscribe(aluno => {
      alert(aluno["mensagem"]);
      this.voltar();
    });
  }

  alterar() {
    this._alunoServico.alterar(this.aluno).subscribe(aluno => {
      alert(aluno["mensagem"]);
      this._router.navigate(["/aluno"]);
    });
  }
}

/*

incluir() {
    this.cursoService.incluir(this.curso).subscribe(retorno => {
      console.log(retorno);
      alert(retorno["mensagem"]);
      this.voltar();
    });
  }

*/
