import { Component, OnInit } from "@angular/core";
import { Curso } from "../servico/curso";
import { CursoService } from "../servico/curso.service";
import { Router, ActivatedRoute } from "@angular/router";
import { async } from "@angular/core/testing";

@Component({
  selector: "app-curso-manter",
  templateUrl: "./curso-manter.component.html",
  styleUrls: ["./curso-manter.component.scss"]
})
export class CursoManterComponent implements OnInit {
  curso: Curso = new Curso();

  nomeCurso: string = "";
  operacao: string = "incluir";

  constructor(
    private cursoService: CursoService,
    private _router: Router,
    private _routeActivated: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.nomeCurso = this._routeActivated.snapshot.params.id;
    if (this.nomeCurso != null) {
      this.operacao = "Alterar";
      this.cursoService.pesquisar(this.nomeCurso).subscribe(data => {
        this.curso = (<Curso[]>data)[0];
      });
    }
  }

  incluir() {
    this.cursoService.incluir(this.curso).subscribe(retorno => {
      console.log(retorno);
      alert(retorno["mensagem"]);
      this.voltar();
    });
  }

  voltar() {
    this._router.navigate(["/curso"]);
  }

  alterar(){
    this.cursoService.alterar(this.curso).subscribe(
      data => {
        alert(data['mensagem']);
        this._router.navigate(['/curso']);        
      }
    );
  }
}
