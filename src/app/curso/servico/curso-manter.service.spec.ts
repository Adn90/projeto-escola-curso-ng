import { TestBed } from '@angular/core/testing';

import { CursoManterService } from './curso-manter.service';

describe('CursoManterService', () => {
  let service: CursoManterService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CursoManterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
