export class Curso {
    nome: string = '';
    cargaHoraria:number;
    dataInicio: Date;
    instrutor: string;
    local: string;
}