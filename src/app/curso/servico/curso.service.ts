import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Curso } from "./curso";
//singleton
@Injectable({
  providedIn: "root"
})
export class CursoService {
  // esse parâmetro serve com singleton
  constructor(private _http: HttpClient) {}

  pesquisar(nome: string) {
    return this._http.get(
      "https://cors-anywhere.herokuapp.com/https://stormy-badlands-29216.herokuapp.com/api/curso/consultar/" +
        nome
    );
  }

  incluir(curso: Curso) {
    return this._http.post(
      "https://cors-anywhere.herokuapp.com/https://stormy-badlands-29216.herokuapp.com/api/curso/incluir",
      curso
    );
  }

  remover(curso: Curso) {
    return this._http.post("https://cors-anywhere.herokuapp.com/https://stormy-badlands-29216.herokuapp.com/api/curso/remover", curso);
  }

  alterar(curso: Curso){        
    return this._http.patch("https://cors-anywhere.herokuapp.com/https://stormy-badlands-29216.herokuapp.com/api/curso/alterarparcial", curso);
  }
}