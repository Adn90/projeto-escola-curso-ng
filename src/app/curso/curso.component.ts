import { Component, OnInit } from "@angular/core";
import { Curso } from "./servico/curso";
import { CursoService } from "./servico/curso.service";
import { Router } from '@angular/router';

@Component({
  selector: "app-curso",
  templateUrl: "./curso.component.html",
  styleUrls: ["./curso.component.scss"]
})
export class CursoComponent implements OnInit {
  curso = new Curso();
  selecionado: Curso;

  cursos: Curso[] = [];

  constructor(
    private _router: Router,
    private _cursoServico: CursoService) {}

  ngOnInit(): void {
    this.msg();
  }

  msg() {
    this._cursoServico.pesquisar(this.curso.nome).subscribe((retorno: Curso[]) => {
      this.cursos  = retorno;
    });
  }

  incluir() {
    this._router.navigate(['/curso/incluir']);
  }

  selecionar(valor) {
    this.selecionado = valor;

  }

  remover() {
    this._cursoServico.remover(this.selecionado).subscribe(data => {
      alert(data['mensagem']);
    });
  }

  alterar() {
    this._router.navigate(['/curso/alterar/'+this.selecionado.nome]);
  }
}
